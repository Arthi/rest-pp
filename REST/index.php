<?php
/**
 * Created by PhpStorm.
 * User: kpawla13
 * Date: 10.11.2017
 * Time: 13:08
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE, HEAD');

date_default_timezone_set('Europe/Warsaw');
setlocale(LC_ALL, 'pl_PL.utf8');

require 'vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Http\Response as Response;

$app = new \Slim\App;

$app->get('/students[/{index}]', function (Request $request, Response $response) {

    $sc = new StudenciController();

    $index = $request->getAttribute('index');

    $imie_request = isset($_GET['imie']) ? $_GET['imie'] : null;
    $nazwisko_request = isset($_GET['nazwisko']) ? $_GET['nazwisko'] : null;
    $bornAt_request = isset($_GET['bornAt']) ? $_GET['bornAt'] : null;
    $bornBefore_request = isset($_GET['bornBefore']) ? $_GET['bornBefore'] : null;
    $bornAfter_request = isset($_GET['bornAfter']) ? $_GET['bornAfter'] : null;

    $resp = $index != null ? $sc->getStudent($index) : $sc->getAllStudents($imie_request, $nazwisko_request, $bornAt_request, $bornBefore_request, $bornAfter_request);

    return Utils::Respond($request, $response, $resp);

});

$app->get('/students/{index}/grades', function (Request $request, Response $response) {

    $greaterThan_request = isset($_GET['greaterThan']) ? $_GET['greaterThan'] : null;
    $lesserThan_request = isset($_GET['lesserThan']) ? $_GET['lesserThan'] : null;

    $oc = new OcenyController();

    $resp = $oc->getStudentGrades($request->getAttribute('index'), $greaterThan_request, $lesserThan_request);

    return Utils::Respond($request, $response, $resp);

});

$app->get('/students/{index}/subjects[/{subject}]', function (Request $request, Response $response) {

    $pc = new PrzedmiotyController();

    $sub = $request->getAttribute('subject');

    if ($sub != null) $resp = $pc->getSubject($sub);
    else $resp = $pc->getSubjectsForStudent($request->getAttribute('index'));

    return Utils::Respond($request, $response, $resp);

});

$app->get('/students/{index}/subjects/{subject}/grades', function (Request $request, Response $response) {

    $oc = new OcenyController();

    $sub = $request->getAttribute('subject');
    $stud = $request->getAttribute('index');

    $resp = $oc->getGradesFromSubjectForStudent($stud, $sub);

    return Utils::Respond($request, $response, $resp);

});

$app->get('/subjects[/{subject}]', function (Request $request, Response $response) {

    $prowadzacy_request = isset($_GET['teacher']) ? $_GET['teacher'] : null;

    $pc = new PrzedmiotyController();

    $id = $request->getAttribute('subject');

    $resp = $id != null ? $pc->getSubject($id) : $pc->getAllSubjects($prowadzacy_request);

    return Utils::Respond($request, $response, $resp);

});

$app->get('/grades[/{grade}]', function (Request $request, Response $response) {

    $oc = new OcenyController();

    $id = $request->getAttribute('grade');

    $resp = $id != null ? $oc->getGrade($id) : $oc->getAllGrades();

    return Utils::Respond($request, $response, $resp);

});

$app->get('/subjects/{subject}/grades', function (Request $request, Response $response) {

    $oc = new OcenyController();

    $resp = $oc->getGradesFromSubject($request->getAttribute('subject'));

    return Utils::Respond($request, $response, $resp);

});

$app->post('/students', function (Request $request, Response $response) {

    $data = json_decode($request->getBody(), true);
    if ($data === null) $data = $request->getParsedBody();
    $data = (object) $data;

    $sc = new StudenciController();

    $resp = $sc->createStudent($data);

    return Utils::Respond($request, $response, $resp);

});

$app->post('/subjects', function (Request $request, Response $response) {

    $data = json_decode($request->getBody(), true);
    if ($data === null) $data = $request->getParsedBody();
    $data = (object) $data;

    $pc = new PrzedmiotyController();

    $resp = $pc->createSubject($data);

    return Utils::Respond($request, $response, $resp);

});

$app->put('/students/{index}/subjects/{subject}', function (Request $request, Response $response) {

    $sc = new StudenciController();

    $student = $request->getAttribute('index');
    $przedmiot = $request->getAttribute('subject');

    if ($_GET['sign'] == 'in') {

        $resp = $sc->signInStudentForSubject($student, $przedmiot);

    }
    else if ($_GET['sign'] == 'out') {

        $resp = $sc->signOutStudentFromSubject($student, $przedmiot);

    }
    else {
        $resp = Utils::prepareResponse(null, 409);
    }

    return Utils::Respond($request, $response, $resp);

});

//$app->post('/grades', function (Request $request, Response $response) {
//
//    $data = json_decode($request->getBody(), true);
//    if ($data === null) $data = $request->getParsedBody();
//    $data = (object) $data;
//
//    $oc = new OcenyController();
//
//    if (!in_array($data->wartosc, array(2,2.5,3,3.5,4,4.5,5))) {
//        $resp = Utils::prepareResponse(false, 409); //400?
//    }
//    else $resp = $oc->createGrade($data);
//
//    return Utils::Respond($request, $response, $resp);
//
//});

$app->post('/students/{index}/subjects/{subject}/grades', function (Request $request, Response $response) {

    $data = json_decode($request->getBody(), true);
    if ($data === null) $data = $request->getParsedBody();
    $data = (object) $data;

    $oc = new OcenyController();

    $student = $request->getAttribute('index');
    $przedmiot = $request->getAttribute('subject');

    if (!in_array($data->wartosc, array(2,2.5,3,3.5,4,4.5,5))) {
        $resp = Utils::prepareResponse(false, 409);
    }
    else $resp = $oc->createGrade($data, $student, $przedmiot);

    return Utils::Respond($request, $response, $resp);

});

$app->put('/students/{index}', function (Request $request, Response $response) {

    $data = json_decode($request->getBody(), true);
    if ($data === null) $data = $request->getParsedBody();
    $data = (object) $data;

    $sc = new StudenciController();

    $resp = $sc->updateStudent($request->getAttribute('index'), $data);

    return Utils::Respond($request, $response, $resp);

});

$app->put('/subjects/{subject}', function (Request $request, Response $response) {

    $data = json_decode($request->getBody(), true);
    if ($data === null) $data = $request->getParsedBody();
    $data = (object) $data;

    $pc = new PrzedmiotyController();

    $resp = $pc->updateSubject($request->getAttribute('subject'), $data);

    return Utils::Respond($request, $response, $resp);

});

$app->put('/grades/{grade}', function (Request $request, Response $response) {

    $data = json_decode($request->getBody(), true);
    if ($data === null) $data = $request->getParsedBody();
    $data = (object) $data;

    $pc = new OcenyController();

    $resp = $pc->updateGrade($request->getAttribute('grade'), $data);

    return Utils::Respond($request, $response, $resp);

});

$app->delete('/students/{index}', function (Request $request, Response $response) {

    $sc = new StudenciController();

    $resp = $sc->deleteStudent($request->getAttribute('index'));

    return Utils::Respond($request, $response, $resp);

});

$app->delete('/subjects/{subject}', function (Request $request, Response $response) {

    $pc = new PrzedmiotyController();

    $resp = $pc->deleteSubject($request->getAttribute('subject'));

    return Utils::Respond($request, $response, $resp);

});

$app->delete('/grades/{grade}', function (Request $request, Response $response) {

    $pc = new OcenyController();

    $resp = $pc->deleteGrade($request->getAttribute('grade'));

    return Utils::Respond($request, $response, $resp);

});

$app->run();