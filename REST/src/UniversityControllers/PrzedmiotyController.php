<?php
/**
 * Created by PhpStorm.
 * User: karol.pawlak
 * Date: 24.03.2018
 * Time: 13:02
 */

class PrzedmiotyController
{

    public function getAllSubjects($teacher) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $filters = [];

        if ($teacher != null) $filters['prowadzacy'] = $teacher;

        $przedmioty = $client->REST->przedmioty->find($filters);

        $list = array();

        foreach ($przedmioty as $przedmiot) {

            $list[] = new Przedmiot($przedmiot['id'], $przedmiot['nazwa'], $przedmiot['prowadzacy']);

        }

        if (count($list) > 0) {

            $castedStudents = array_map("Utils::customObjectToJSON", $list);
            return Utils::prepareResponse($castedStudents, 200);
        }

        return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

    }

    public function getSubject($subject_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $przedmiotyRow = $client->REST->przedmioty->findOne(['id' => intval($subject_id)]);

        if ($przedmiotyRow == null) {

            return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

        }

        $przedmiot = new Przedmiot($przedmiotyRow['id'], $przedmiotyRow['nazwa'], $przedmiotyRow['prowadzacy']);

        $castedSubject = Utils::customObjectToJSON($przedmiot);

        return Utils::prepareResponse($castedSubject, 200);

    }

    public function getSubjectsForStudent($student_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        //$oceny = $client->REST->oceny->find(['student_id' => intval($student_id)]);

        //$ids = array();

        //foreach($oceny as $ocena) array_push($ids, intval($ocena->przedmiot_id));

        $subjects = $client->REST->studenci->findOne(['index' => intval($student_id)]);

        //$przedmioty = $client->REST->przedmioty->find(['id' => ['$in' => $ids]]);
        $przedmioty = $client->REST->przedmioty->find(['_id' => ['$in' => $subjects['przedmioty']]]);

        $list = array();

        foreach ($przedmioty as $przedmiot) {

            $list[] = new Przedmiot($przedmiot['id'], $przedmiot['nazwa'], $przedmiot['prowadzacy']);

        }

        if (count($list) > 0) {

            $castedStudents = array_map("Utils::customObjectToJSON", $list);
            return Utils::prepareResponse($castedStudents, 200);
        }

        return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

    }

    public function createSubject($data) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $old = $client->REST->przedmioty->findOne([], ['sort' => ['id' => -1]]);

        $fresh_id = intval($old->id)+1;

        $przedmiot = array('id' => $fresh_id, 'nazwa' => $data->nazwa, 'prowadzacy' => $data->prowadzacy);

        $prze = $client->REST->przedmioty->insertOne($przedmiot);

        return Utils::prepareResponse($fresh_id, 201, array(array('name' => 'Location', 'value'=>'/subjects/'.$fresh_id)));
    }

    public function updateSubject($subject_id, $data) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $client->REST->przedmioty->updateOne(['id' => intval($subject_id)], ['$set' => array('nazwa' => $data->nazwa, 'prowadzacy' => $data->prowadzacy)]);

        return Utils::prepareResponse(null, 200);

    }

    public function deleteSubject($subject_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $res = $client->REST->przedmioty->deleteOne(['id' => intval($subject_id)]);

        if ($res->getDeletedCount() > 0) return Utils::prepareResponse(true, 200);

        return Utils::prepareResponse(false, 204);

    }

}