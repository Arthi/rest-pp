<?php
/**
 * Created by PhpStorm.
 * User: karol.pawlak
 * Date: 24.03.2018
 * Time: 13:02
 */

class StudenciController
{

    public function getAllStudents($imie, $nazwisko, $at, $before, $after) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $filters = [];

        if ($imie != null) $filters['imie'] = $imie;
        if ($nazwisko != null) $filters['nazwisko'] = $nazwisko;

        if ($at != null) {
            $filters['dataUrodzenia'] = $at;
        }
        else {
            if ($before != null) $filters['dataUrodzenia']['$lt'] = $before;
            if ($after != null) $filters['dataUrodzenia']['$gt'] = $after;
        }

        $students = $client->REST->studenci->find($filters);

        $list = array();

        foreach ($students as $student) {

            $list[] = new Student($student['index'], $student['imie'], $student['nazwisko'], $student['dataUrodzenia']);

        }

        if (count($list) > 0) {

            $castedStudents = array_map("Utils::customObjectToJSON", $list);
            return Utils::prepareResponse($castedStudents, 200);
        }

        return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

    }

    public function getStudent($index) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $studentRow = $client->REST->studenci->findOne(['index' => intval($index)]);

        if ($studentRow == null) {

            return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

        }
        else {

            $student = new Student($studentRow['index'], $studentRow['imie'], $studentRow['nazwisko'], $studentRow['dataUrodzenia']);

            $result = Utils::customObjectToJSON($student);
            return Utils::prepareResponse($result, 200);
        }

    }

    public function createStudent($data) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $old = $client->REST->studenci->findOne([], ['sort' => ['index' => -1]]);

        $fresh_id = intval($old->index)+1;

        $student = array('index' => $fresh_id,'imie' => $data->imie, 'nazwisko' => $data->nazwisko, 'dataUrodzenia' => $data->dataUrodzenia, 'przedmioty' => []);

        $stu = $client->REST->studenci->insertOne($student);

        return Utils::prepareResponse($fresh_id, 201, array(array('name' => 'Location', 'value'=>'/students/'.$fresh_id)));
    }

    public function signInStudentForSubject($student, $subject) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $sub = $client->REST->przedmioty->findOne(['id' => intval($subject)]);

        $client->REST->studenci->updateOne(['index' => intval($student)], ['$addToSet' => ['przedmioty' => $sub['_id']]]);

        return Utils::prepareResponse(null, 200);

    }

    public function signOutStudentFromSubject($student, $subject) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $sub = $client->REST->przedmioty->findOne(['id' => intval($subject)]);

        $client->REST->studenci->updateOne(['index' => intval($student)], ['$pull' => ['przedmioty' => $sub['_id']]]);

        return Utils::prepareResponse(null, 200);

    }

    public function updateStudent($student_id, $data) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $client->REST->studenci->updateOne(['index' => intval($student_id)], ['$set' => array('imie' => $data->imie, 'nazwisko' => $data->nazwisko, 'dataUrodzenia' => $data->dataUrodzenia)]);

        return Utils::prepareResponse(null, 200);

    }

    public function deleteStudent($student_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $res = $client->REST->studenci->deleteOne(['index' => intval($student_id)]);

        if ($res->getDeletedCount() > 0) return Utils::prepareResponse(true, 200);

        return Utils::prepareResponse(false, 204);

    }

}