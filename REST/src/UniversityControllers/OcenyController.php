<?php
/**
 * Created by PhpStorm.
 * User: karol.pawlak
 * Date: 24.03.2018
 * Time: 13:02
 */

class OcenyController
{

    public function getGradesFromSubject($subject_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $oceny = $client->REST->oceny->find(['przedmiot_id' => intval($subject_id)]);

        $list = array();

        foreach ($oceny as $ocena) {

            $list[] =  new Ocena($ocena['id'], $ocena['wartosc'], $ocena['dataWystawienia'], $ocena['student_id'], $ocena['przedmiot_id']);

        }

        if (count($list) > 0) {

            $castedGrades = array_map("Utils::customObjectToJSON", $list);
            return Utils::prepareResponse($castedGrades, 200);
        }

        return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

    }

    public function getGradesFromSubjectForStudent($student_id, $subject_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $oceny = $client->REST->oceny->find(['przedmiot_id' => intval($subject_id), 'student_id' => intval($student_id)]);

        $list = array();

        foreach ($oceny as $ocena) {

            $list[] =  new Ocena($ocena['id'], $ocena['wartosc'], $ocena['dataWystawienia'], $ocena['student_id'], $ocena['przedmiot_id']);

        }

        if (count($list) > 0) {

            $castedGrades = array_map("Utils::customObjectToJSON", $list);
            return Utils::prepareResponse($castedGrades, 200);
        }

        return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

    }

    public function getStudentGrades($student_id, $gt, $lt) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $stud = $client->REST->studenci->findOne(['index' => intval($student_id)]);

        $filters = [];

        $filters['student_db_id'] = $stud['_id'];

        if ($gt != null) $filters['wartosc']['$gt'] = intval($gt);
        if ($lt != null) $filters['wartosc']['$lt'] = intval($lt);

        $oceny = $client->REST->oceny->find($filters);

        $list = array();

        foreach ($oceny as $ocena) {

            $list[] =  new Ocena($ocena['id'], $ocena['wartosc'], $ocena['dataWystawienia'], $ocena['student_id'], $ocena['przedmiot_id']);

        }

        if (count($list) > 0) {

            $castedGrades = array_map("Utils::customObjectToJSON", $list);
            return Utils::prepareResponse($castedGrades, 200);
        }

        return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

    }

    public function getAllGrades() {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $oceny = $client->REST->oceny->find();

        $list = array();

        foreach ($oceny as $ocena) {

            $list[] =  new Ocena($ocena['id'], $ocena['wartosc'], $ocena['dataWystawienia'], $ocena['student_id'], $ocena['przedmiot_id']);

        }

        if (count($list) > 0) {

            $castedGrades = array_map("Utils::customObjectToJSON", $list);
            return Utils::prepareResponse($castedGrades, 200);
        }

        return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

    }

    public function getGrade($grade_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $ocenaRow = $client->REST->oceny->findOne(['id' => intval($grade_id)]);

        if($ocenaRow == null) {

            return Utils::prepareResponse(null, 404, null, 1, 'ITEM_NOT_FOUND');

        }

        $ocena = new Ocena($ocenaRow['id'], $ocenaRow['wartosc'], $ocenaRow['dataWystawienia'], $ocenaRow['student_id'], $ocenaRow['przedmiot_id']);

        $castedGrade = Utils::customObjectToJSON($ocena);
        return Utils::prepareResponse($castedGrade, 200);

    }

//    public function createGrade($data) {
//
//        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");
//
//        $old = $client->REST->oceny->findOne([], ['sort' => ['id' => -1]]);
//
//        $fresh_id = intval($old->id)+1;
//
//        $grade = array('id' => $fresh_id, 'wartosc' => $data->wartosc, 'dataWystawienia' => $data->dataWystawienia, 'student_id' => $data->indexStudenta, 'przedmiot_id' => $data->przedmiot);
//
//        $oce = $client->REST->oceny->insertOne($grade);
//
//        return Utils::prepareResponse($fresh_id, 201, array(array('name' => 'Location', 'value'=>'/grades/'.$fresh_id)));
//    }

    public function createGrade($data, $student_id, $subject_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $dbStudent = $client->REST->studenci->findOne(['index' => intval($student_id)]);

        $dbSubject = $client->REST->przedmioty->findOne(['id' => intval($subject_id)]);

        $old = $client->REST->oceny->findOne([], ['sort' => ['id' => -1]]);

        $fresh_id = intval($old->id)+1;

        $grade = array(
            'id' => $fresh_id,
            'wartosc' => $data->wartosc,
            'dataWystawienia' => $data->dataWystawienia,
            'student_id' => intval($student_id),//$data->indexStudenta,
            'przedmiot_id' => intval($subject_id),//$data->przedmiot,
            'przedmiot_db_id' => $dbSubject['_id'],
            'student_db_id' => $dbStudent['_id']
        );

        $oce = $client->REST->oceny->insertOne($grade);

        return Utils::prepareResponse($fresh_id, 201, array(array('name' => 'Location', 'value'=>'/grades/'.$fresh_id)));
    }

    public function updateGrade($grade_id, $data) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $client->REST->oceny->updateOne(['id' => intval($grade_id)], ['$set' => array('wartosc' => $data->wartosc, 'dataWystawienia' => $data->dataWystawienia, 'student_id' => $data->indexStudenta, 'przedmiot_id' => $data->przedmiot)]);

        return Utils::prepareResponse(null, 200);

    }

    public function deleteGrade($grade_id) {

        $client = new MongoDB\Client("mongodb://192.168.36.16:8004");

        $res = $client->REST->oceny->deleteOne(['id' => intval($grade_id)]);

        if ($res->getDeletedCount() > 0) return Utils::prepareResponse(true, 200);

        return Utils::prepareResponse(false, 204);

    }
}