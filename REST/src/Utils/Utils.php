<?php
/**
 * Created by PhpStorm.
 * User: Arthi
 * Date: 2018-03-24
 * Time: 14:51
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Http\Response as Response;

class Utils {

    public static function prepareResponse($data, $http_code = 200, $headers = array(), $code = 0, $message = "NO_ERROR")
    {

        return array(
            'return' => array(
                'data' => $data,
                'error' => array(
                    'code' => $code,
                    'message' => $message
                )
            ),
            'http_code' => $http_code,
            'headers' => $headers
        );
    }

    public static function customObjectToJSON($temp)
    {
        $temp = (array)$temp;

        $array = array();

        foreach ($temp as $k => $v) {
            $k = preg_match('/^\x00(?:.*?)\x00(.+)/', $k, $matches) ? $matches[1] : $k;
            $array[$k] = $v;

        }

        return $array;
    }

    public static function ArrayToXML( $data, SimpleXMLElement &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            if( is_array($value) ) {
                $subnode = $xml_data->addChild($key);
                self::ArrayToXML($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    public static function Respond(Request $request, Response $response, $resp) {

        if (isset($resp['headers']) && count($resp['headers']) > 0) {
            foreach ($resp['headers'] as $header) {
                $response = $response->withAddedHeader($header['name'], $header['value']);
            }
        }

        $val = $request->getHeader('Accept');

        $accepts = explode(', ', $val[0]);

        if (count($accepts) > 1 && !in_array('application/json', $accepts)) {

            if (in_array('application/xml', $accepts)) {

                $xml_data = new SimpleXMLElement('<?xml version="1.0"?><response></response>');

                Utils::ArrayToXML($resp['return'], $xml_data);

                return $response->write($xml_data->asXML())->withAddedHeader('Content-Type', 'application/xml;charset=utf-8')->withStatus($resp['http_code']);

            } else {
                $resp = Utils::prepareResponse(null, 406);
            }
        }

        return $response->withJson($resp['return'])->withAddedHeader('Content-Type', 'application/json;charset=utf-8')->withStatus($resp['http_code']);

    }

}