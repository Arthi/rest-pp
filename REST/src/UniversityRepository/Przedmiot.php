<?php
/**
 * Created by PhpStorm.
 * User: karol.pawlak
 * Date: 24.03.2018
 * Time: 13:02
 */

class Przedmiot
{
    private $id;
    private $nazwa;
    private $prowadzacy;

    /**
     * Przedmiot constructor.
     * @param $id - identyfikator przedmiotu
     * @param $nazwa - nazwa przedmiotu
     * @param $prowadzacy - imię i nazwisko osoby prowadzącej ten przedmiot
     */
    public function __construct($id, $nazwa, $prowadzacy)
    {
        $this->id = $id;
        $this->nazwa = $nazwa;
        $this->prowadzacy = $prowadzacy;
    }

}