<?php
/**
 * Created by PhpStorm.
 * User: karol.pawlak
 * Date: 24.03.2018
 * Time: 13:02
 */

class Ocena
{
    private $id;
    private $wartosc;
    private $dataWystawienia;
    private $student;
    private $przedmiot;

    /**
     * Ocena constructor.
     * @param $id - identyfikator oceny w systemie
     * @param $wartosc - wartość oceny (pomiędzy 2.0 a 5.0)
     * @param $dataWystawienia - data wystawienia oceny
     * @param $student - referencja na obiekt studenta do którego należy ocena
     * @param $przedmiot - id przedmiotu z którzego pochodzi ocena
     */
    public function __construct($id, $wartosc, $dataWystawienia, $student, $przedmiot)
    {
        $this->id = $id;
        $this->wartosc = $wartosc;
        $this->dataWystawienia = $dataWystawienia;
        $this->student = $student;
        $this->przedmiot = $przedmiot;
    }

}