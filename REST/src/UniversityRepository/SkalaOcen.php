<?php
/**
 * Created by PhpStorm.
 * User: karol.pawlak
 * Date: 24.03.2018
 * Time: 13:02
 */

abstract class SkalaOcen
{
    public static $DWA = 2;
    public static $DWA_I_POL = 2.5;
    public static $TRZY = 3;
    public static $TRZY_I_POL = 3.5;
    public static $CZTERY = 4;
    public static $CZTERY_I_POL = 4.5;
    public static $PIEC = 5;

}