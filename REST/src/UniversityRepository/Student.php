<?php
/**
 * Created by PhpStorm.
 * User: karol.pawlak
 * Date: 24.03.2018
 * Time: 13:02
 */

class Student
{
    private $index;
    private $imie;
    private $nazwisko;
    private $dataUrodzenia;

    /**
     * Student constructor.
     * @param $index - indeks studenta
     * @param $imie - imie studenta
     * @param $nazwisko - nazwisko studenta
     * @param $dataUrodzenia - data urodzenia studenta
     */
    public function __construct($index, $imie, $nazwisko, $dataUrodzenia)
    {
        $this->index = $index;
        $this->imie = $imie;
        $this->nazwisko = $nazwisko;
        $this->dataUrodzenia = $dataUrodzenia;
    }

}