/**
 * Created by kpawla13 on 2017-03-06.
 */

"use strict";

let allowedGrades = [2, 2.5, 3, 3.5, 4, 4.5, 5];
let students = {
    editingStudent: null,
    studentsList: [],
    addStudent: function () {

        function isDate(val) {
            let d = new Date(val);
            return !isNaN(d.valueOf());
        }

        if ($('#imie').val().length < 3) {
            alert('Imię musi być dłuższe niż 2 znaki');
            return;
        }
        if ($('#nazwisko').val().length < 3) {
            alert('Nazwisko musi być dłuższe niż 2 znaki');
            return;
        }
        if (!$('#data').val()) {

            alert('Musisz podać datę urodzenia');
            return;

        }
        if (!isDate($('#data').val())) {
            alert('Data jest niepoprawna');
            return;
        }

        $.ajax({
            url: "http://192.168.36.16/karol/REST/students",
            method: 'POST',
            dataType: 'json',
            data: {
                'imie': $('#imie').val(),
                'nazwisko': $('#nazwisko').val(),
                'dataUrodzenia': $('#data').val(),
            },
            success: function (data) {

                students.studentsList.push({
                    index: ko.observable(data.data),
                    imie: ko.observable($('#imie').val()),
                    nazwisko: ko.observable($('#nazwisko').val()),
                    dataUrodzenia: ko.observable($('#data').val())
                });

                $('#imie').val('');
                $('#nazwisko').val('');
                $('#data').val('');

            },
            error: function (data) {

                console.log(data);

            }
        });

    },
    showDetailsOfStudent: function (student) {

        $('#student-details-index').html(student.index());
        $('#student-details-name').html(student.imie());
        $('#student-details-lastname').html(student.nazwisko());
        $('#student-details-date').html(student.dataUrodzenia());
        window.location = '#students-tile-details';
    },
    showEditOfStudent: function (student) {

        students.editingStudent = student;
        $('#student-edit-index').val(student.index());
        $('#student-edit-name').val(student.imie());
        $('#student-edit-lastname').val(student.nazwisko());
        $('#student-edit-date').val(student.dataUrodzenia());
        window.location = '#students-tile-edit';

    },
    updateStudent: function () {

        function isDate(val) {
            let d = new Date(val);
            return !isNaN(d.valueOf());
        }

        if (students.editingStudent != null) {

            if ($('#student-edit-name').val().length < 3) {
                alert('Imię musi być dłuższe niż 2 znaki');
                return;
            }
            if ($('#student-edit-lastname').val().length < 3) {
                alert('Nazwisko musi być dłuższe niż 2 znaki');
                return;
            }
            if (!$('#student-edit-date').val()) {
                alert('Musisz podać datę urodzenia');
                return;
            }
            if (!isDate($('#student-edit-date').val())) {
                alert('Data jest niepoprawna');
                return;
            }

            $.ajax({
                url: "http://192.168.36.16/karol/REST/students/" + $('#student-edit-index').val(),
                method: 'PUT',
                dataType: 'json',
                data: {
                    'index': students.editingStudent.index(),
                    'imie': $('#student-edit-name').val(),
                    'nazwisko': $('#student-edit-lastname').val(),
                    'dataUrodzenia': $('#student-edit-date').val()
                },
                success: function (data) {

                    students.editingStudent.imie($('#student-edit-name').val());
                    students.editingStudent.nazwisko($('#student-edit-lastname').val());
                    students.editingStudent.dataUrodzenia($('#student-edit-date').val());
                    window.location.replace('#students-tile');

                },
                error: function (data) {

                    console.log(data);

                }
            });

        }

    },
    removeStudent: function (student) {

        $.ajax({
            url: "http://192.168.36.16/karol/REST/students/" + student.index(),
            method: 'DELETE',
            dataType: 'json',
            success: function (data) {

                students.studentsList.remove(student);

            },
            error: function (data) {

                console.log(data);

            }
        });

    },
};
let subjects = {
    editingSubject: null,
    subjectsList: [],
    addSubject: function () {

        if ($('#nazwa').val().length < 3) {
            alert('Nazwa musi być dłuższa niż 2 znaki');
            return;
        }
        if ($('#prowadzacy').val().length < 6) {
            alert('Imię i nazwisko prowadzącego musi być dłuższe niż 5 znaków');
            return;
        }

        $.ajax({
            url: "http://192.168.36.16/karol/REST/subjects",
            method: 'POST',
            dataType: 'json',
            data: {
                'nazwa': $('#nazwa').val(),
                'prowadzacy': $('#prowadzacy').val()
            },
            success: function (data) {

                subjects.subjectsList.push({
                    id: ko.observable(data.data),
                    nazwa: ko.observable($('#nazwa').val()),
                    prowadzacy: ko.observable($('#prowadzacy').val())
                });

                $('#nazwa').val('');
                $('#prowadzacy').val('');

            },
            error: function (data) {

                console.log(data);

            }
        });

    },
    showDetailsOfSubject: function (subject) {

        $('#subject-details-id').html(subject.id());
        $('#subject-details-nazwa').html(subject.nazwa());
        $('#subject-details-prowadzacy').html(subject.prowadzacy());
        window.location = '#subjects-tile-details';
    },
    showEditOfSubject: function (subject) {

        subjects.editingSubject = subject;
        $('#subject-edit-id').val(subject.id());
        $('#subject-edit-nazwa').val(subject.nazwa());
        $('#subject-edit-prowadzacy').val(subject.prowadzacy());
        window.location = '#subjects-tile-edit';

    },
    updateSubject: function () {

        if (subjects.editingSubject != null) {

            if ($('#subject-edit-nazwa').val().length < 3) {
                alert('Nazwa musi być dłuższa niż 2 znaki');
                return;
            }
            if ($('#subject-edit-prowadzacy').val().length < 6) {
                alert('Imię i nazwisko prowadzącego musi być dłuższe niż 5 znaków');
                return;
            }

            $.ajax({
                url: "http://192.168.36.16/karol/REST/subjects/" + $('#subject-edit-id').val(),
                method: 'PUT',
                dataType: 'json',
                data: {
                    'nazwa': $('#subject-edit-nazwa').val(),
                    'prowadzacy': $('#subject-edit-prowadzacy').val()
                },
                success: function (data) {

                    subjects.editingSubject.nazwa($('#subject-edit-nazwa').val());
                    subjects.editingSubject.prowadzacy($('#subject-edit-prowadzacy').val());
                    window.location.replace('#subjects-tile');

                },
                error: function (data) {

                    console.log(data);

                }
            });

        }

    },
    removeSubject: function (subject) {

        $.ajax({
            url: "http://192.168.36.16/karol/REST/subjects/" + subject.id(),
            method: 'DELETE',
            dataType: 'json',
            success: function (data) {

                subjects.subjectsList.remove(subject);

            },
            error: function (data) {

                console.log(data);

            }
        });

    },
};
let grades = {
    editingGrade: null,
    gradesList: [],
    addGrade: function () {

        function isDate(val) {
            let d = new Date(val);
            return !isNaN(d.valueOf());
        }

        if (allowedGrades.indexOf(parseFloat($('#wartosc').val())) < 0) {
            alert('Ocena musi być jadną z: 2, 2.5, 3, 3.5, 4, 4.5, 5');
            return;
        }
        if (!$('#data-wystawienia').val()) {
            alert('Musisz podać datę wystawienia oceny');
            return;
        }
        if (!isDate($('#data-wystawienia').val())) {
            alert('Data jest niepoprawna');
            return;
        }

        $.ajax({
            url: "http://192.168.36.16/karol/REST/students/" + $('#grade-for-student').val() + "/subjects/" + $('#grade-for-subject').val() + "/grades",
            method: 'POST',
            dataType: 'json',
            data: {
                'wartosc': $('#wartosc').val(),
                'dataWystawienia': $('#data-wystawienia').val()
            },
            success: function (data) {

                grades.gradesList.push({
                    id: ko.observable(data.data),
                    wartosc: ko.observable($('#wartosc').val()),
                    dataWystawienia: ko.observable($('#data-wystawienia').val()),
                    student: ko.observable($("#grade-for-student").val()),
                    przedmiot: ko.observable($("#grade-for-subject").val()),
                });

                $('#wartosc').val('');
                $('#data-wystawienia').val('');

            },
            error: function (data) {

                console.log(data);

            }
        });

    },
    showDetailsOfGrade: function (grade) {

        $('#grades-details-id').html(grade.id());
        $('#grades-details-wartosc').html(grade.wartosc());
        $('#grades-details-data-wystawienia').html(grade.dataWystawienia());
        $('#grades-details-student').html(grades.findStudentForGrade(grade.student));
        $('#grades-details-subject').html(grades.findSubjectForGrade(grade.przedmiot));
        window.location = '#grades-tile-details';
    },
    showEditOfGrade: function (grade) {

        grades.editingGrade = grade;
        $('#grades-edit-id').text(grade.id());
        $('#grades-edit-wartosc').val(grade.wartosc());
        $('#grades-edit-data-wystawienia').val(grade.dataWystawienia());
        $('#grades-edit-student').val(grade.student());
        $('#grades-edit-subject').val(grade.przedmiot());
        window.location = '#grades-tile-edit';

    },
    updateGrade: function () {

        function isDate(val) {
            let d = new Date(val);
            return !isNaN(d.valueOf());
        }

        if (grades.editingGrade != null) {

            if (allowedGrades.indexOf(parseFloat($('#grades-edit-wartosc').val())) < 0) {
                alert('Ocena musi być jadną z: 2, 2.5, 3, 3.5, 4, 4.5, 5');
                return;
            }
            if (!$('#grades-edit-data-wystawienia').val()) {
                alert('Musisz podać datę wystawienia oceny');
                return;
            }
            if (!isDate($('#grades-edit-data-wystawienia').val())) {
                alert('Data jest niepoprawna');
                return;
            }

            $.ajax({
                url: "http://192.168.36.16/karol/REST/grades/" + $('#grades-edit-id').text(),
                method: 'PUT',
                dataType: 'json',
                data: {
                    'wartosc': $('#grades-edit-wartosc').val(),
                    'dataWystawienia': $('#grades-edit-data-wystawienia').val(),
                    "indexStudenta": $('#grades-edit-student').val(),
                    "przedmiot": $('#grades-edit-subject').val()
                },
                success: function (data) {

                    grades.editingGrade.wartosc($('#grades-edit-wartosc').val());
                    grades.editingGrade.dataWystawienia($('#grades-edit-data-wystawienia').val());
                    grades.editingGrade.student($('#grades-edit-student').val());
                    grades.editingGrade.przedmiot($('#grades-edit-subject').val());
                    window.location.replace('#grades-tile');

                },
                error: function (data) {

                    console.log(data);

                }
            });

        }

    },
    removeGrade: function (grade) {

        $.ajax({
            url: "http://192.168.36.16/karol/REST/grades/" + grade.id(),
            method: 'DELETE',
            dataType: 'json',
            success: function (data) {

                grades.gradesList.remove(grade);

            },
            error: function (data) {

                console.log(data);

            }
        });

    },
    findStudentForGrade: function (searched) {

        let students2 = ko.mapping.toJS(students.studentsList);

        for (let i = 0; i < students2.length; i++) {

            if (parseInt(students2[i].index) == parseInt(searched())) return students2[i].imie + ' ' + students2[i].nazwisko;

        }

        return 'Nieznany student';

    },
    findSubjectForGrade: function (searched) {

        let subjects2 = ko.mapping.toJS(subjects.subjectsList);

        for (let i = 0; i < subjects2.length; i++) {

            if (parseInt(subjects2[i].id) == parseInt(searched())) return subjects2[i].nazwa;

        }

        return 'Nieznany przedmiot';

    }

};
let app = {
    start: function () {

        $.ajax({
            url: "http://192.168.36.16/karol/REST/students",
            method: 'GET',
            dataType: 'json',
            success: function (data) {

                students.studentsList = ko.mapping.fromJS(data.data);

                ko.applyBindings(students, $('#students-tile')[0]);
                ko.applyBindings(students, $('#students-tile-edit')[0]);
                ko.applyBindings(students, $('#students-tile-details')[0]);

                $.ajax({
                    url: "http://192.168.36.16/karol/REST/subjects",
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {

                        subjects.subjectsList = ko.mapping.fromJS(data.data);

                        ko.applyBindings(subjects, $('#subjects-tile')[0]);
                        ko.applyBindings(subjects, $('#subjects-tile-edit')[0]);
                        ko.applyBindings(subjects, $('#subjects-tile-details')[0]);

                        $.ajax({
                            url: "http://192.168.36.16/karol/REST/grades",
                            method: 'GET',
                            dataType: 'json',
                            success: function (data) {

                                grades.gradesList = ko.mapping.fromJS(data.data);
                                grades.studentsList = students.studentsList;
                                grades.subjectsList = subjects.subjectsList;

                                ko.applyBindings(grades, $('#grades-tile')[0]);
                                ko.applyBindings(grades, $('#grades-tile-edit')[0]);
                                ko.applyBindings(grades, $('#grades-tile-details')[0]);

                            },
                            error: function (data) {

                                console.log(data);

                            }
                        });


                    },
                    error: function (data) {

                        console.log(data);

                    }
                });

            },
            error: function (data) {

                console.log(data);

            }
        });

    }
};

$(document).ready(function () {

    app.start();

});
